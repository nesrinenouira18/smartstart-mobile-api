/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import static com.codename1.charts.util.ColorUtil.rgb;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Stroke;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.RoundBorder;
import com.mycompany.Entite.Projet;
import com.mycompany.Service.ServiceProjet;
import java.util.ArrayList;

/**
 *
 * @author bhk
 */
public class Affichage {

    Form f,listForm;
    
  
    public Affichage() {
        f = new Form("Liste Projets");
       
        ServiceProjet service=new ServiceProjet();
        listItems(service.getList2());
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

     public void listItems(ArrayList<Projet> listProjets)
    {
      //listForm = new Form( BoxLayout.y());
      f.removeAll();
      Toolbar tb = f.getToolbar();
      tb.removeAll();
    
     
            Font smallBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
            Font largeBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
            
             Container cccc =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
         
        
            TextArea  proDesc = new TextArea ("");
            proDesc.getAllStyles().setMarginTop(2);
            proDesc.getAllStyles().setMarginBottom(2); 
            
               Button rechercher = new Button();
               rechercher.setText("                   Rechercher                      ");
               rechercher.getAllStyles().setBorder(RoundBorder.create().rectangle(true).stroke(new Stroke(2, Stroke.CAP_SQUARE, Stroke.JOIN_MITER, 4)).
               strokeColor(rgb(156, 91, 87)).strokeOpacity(120));
               rechercher.getAllStyles().setFont(largeBoldProportionalFont);
               rechercher.getAllStyles().setFgColor(0xffffffff);
               rechercher.getAllStyles().setBgColor(rgb(156, 91, 87));
               rechercher.getAllStyles().setMarginBottom(10); 
               cccc.add(proDesc);
               cccc.add(rechercher);
               f.add(cccc);
               String empty = new String();
                rechercher.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if(!proDesc.getText().equals(empty)){
                       Search s = new Search(proDesc.getText());
                       
                        }else{
                        Affichage a = new Affichage();
                        }
                    }
               }); 
            
        
            
            
            
        for(Projet c: listProjets){
            Container c1 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
            TextArea  nom = new TextArea (c.getLibelleProjet());
            TextArea cpt = new TextArea();
            TextArea minBdg = new TextArea(""+c.getBdgMinProjet()+" - "+c.getBdgMaxProjet()+" DT");
            nom.setEditable(false);
            cpt.setEditable(false);
            minBdg.setEditable(false);
              
            c1.getAllStyles().setMarginTop(10);
            c1.getAllStyles().setBorder(Border.createEmpty());
            nom.getAllStyles().setFont(largeBoldProportionalFont);
            cpt.getAllStyles().setFont(smallBoldProportionalFont);
            minBdg.getAllStyles().setFont(smallBoldProportionalFont);
            
            
            nom.getAllStyles().setBgColor(rgb(156, 91, 87));
            nom.getAllStyles().setFgColor(0xffffffff);
            cpt.getAllStyles().setFgColor(rgb(156, 91, 87));
            minBdg.getAllStyles().setFgColor(rgb(156, 91, 87));
            nom.getAllStyles().setBorder(Border.createEmpty());
            cpt.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setAlignment(Component.RIGHT);
             
            nom.getAllStyles().setMarginBottom(0);
            cpt.getAllStyles().setMarginTop(0);
            cpt.getAllStyles().setMarginBottom(0);
            minBdg.getAllStyles().setMarginTop(0);
               
            c1.add(nom);
             if(c.getCptProjet5()==null){
                if(c.getCptProjet4()==null){
                    if(c.getCptProjet3()==null){
                        if(c.getCptProjet2()==null){
                            if(c.getCptProjet1()==null){
                                }else{cpt.setText(""+c.getCptProjet1()); c1.add(cpt);}
                        }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()); c1.add(cpt);}
                    }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()); c1.add(cpt);}
                }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()); c1.add(cpt);}
            }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()+" - "+c.getCptProjet5()); c1.add(cpt);}
            c1.add(minBdg);
            c1.setLeadComponent(nom);
            nom.addPointerPressedListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    projetAffichage pro=new projetAffichage();
                    pro.projetAff(c.getIdProjet());
                }
            });

            f.add(c1);
        }
        
       f.show();
     }
    
    
    
}
