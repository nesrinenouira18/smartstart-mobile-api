/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author NOUIRA Nesrine
 */
public class ServiceUserMob {
    
    public int checkUser(User u)
    { ArrayList<User> listPro = new ArrayList<>();
        listPro = getListUser();
        for(User pro: listPro){
        if(pro.getLogin().equals(u.getLogin()) && pro.getMdp().equals(u.getMdp())){
         return 1;
        }
        }
       return 0;
    }
    
     public ArrayList<User> parseListTaskJson(String json) {

        ArrayList<User> listUser = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
            
            for (Map<String, Object> obj : list) {
                
                User p = new User();
                float id = Float.parseFloat(obj.get("id").toString());
               
                p.setMdp(obj.get("username").toString());
                p.setLogin(obj.get("username").toString());
                p.setId((int) id);
                listUser.add(p);
            }

        } catch (IOException ex) {
        }
        
        return listUser;

    }
    
    
    
    
     ArrayList<User> listUsers = new ArrayList<>();
    
    public ArrayList<User> getListUser(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PiDev/web/app_dev.php/gm/getUsers");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceUserMob ser = new ServiceUserMob();
                listUsers = ser.parseListTaskJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listUsers;
    }
    
   
}
